# FXMLHelper
A plugin for the IntelliJ platform, which adds handy functions to JavaFX Projects

## License
[Apache License 2.0](LICENSE)
